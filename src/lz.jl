using Printf
using LinearAlgebra
using DelimitedFiles

"The overlap integral between wavefunctions `ψ1` and `ψ2`"
overlap(ψ1, ψ2) = abs2(ψ1' * ψ2)

"The expectation value of `o = v * Diagonal(e) * v' with respect to wavefunction
`ψ`"
expectation(e, v, ψ) = abs2(ψ' * v * Diagonal(e) * v' * ψ)

"The inverse participation ratio is defined as ``∑_i |ψ(i)|^4``"
inverse_participation_ratio(ψ) = sum((abs.(ψ)).^4)

"Projection of wavefunction `ψ` onto the adiabatic energy eigenstates"
adiabatic_project(v, ψ) = (abs.(ψ' * v))[1, :]

"The infinite-crossing generalization of a Landau-Zener Hamiltonian"
function h(ω, s, t, n)
    N = 2 * n + 1
    z = diagm(0 => -n:n)
    d = t * Matrix{ComplexF64}(I, N, N)
    h = (s/π) * ones(ComplexF64, N, N)
    return ω * [z+d h; h z-d]
end

"Propagate `ψ` by the small time increment `dt`, from the spectral decomposition
of the Hamiltonian `v * Diagonal(e) * v'`."
function time_propagate(ψ, e, v, dt)
    return v * Diagonal(exp.(-1im*dt*e)) * v' * ψ
end

"Prepare a Gaussian initial state."
function prepare_gaussian_initial_state(ω, s, n, σ)
    N = 2 * n + 1
    _, v = eigen(h(ω, s, 0.0, n))
    site = -(N-1/2):(N-1/2)
    σ = N*σ
    ψ = v * exp.(-(site/σ).^2)
    ψ /= norm(ψ)
    return ψ
end

"Time-evolve a Gaussian state of width σ in time steps dt, using a
time-dependent Hamiltonian

``H_{LZg}(t) = ω (Sz ⊗ 1) + λ t (1 ⊗ σz) + J (A ⊗ σx)``

where ``Sz = diag(..., -1, 0, 1, ...)`` and A is a n x n matrix with 1 in all
slots. This is a generalization of a Landau-Zener crossing.

We rescale time and energy and in the simulation study the Hamiltonian

``H(t) = ω (Sz ⊗ 1) + ω t (1 ⊗ σz) + (ω s / π) (A ⊗ σx)``

The simulation should converge for dt -> 0 and n -> ∞.
"
function time_evolve(; ω=1, s=0.1, σ=0.01, t_end=100, dt=0.05, n=200,
                       path="data", measure_every=1)

    # create directories and reset/set CSV column names
    try
        mkdir(path)
    catch SystemError
        nothing
    end
    open("$path/measurements.csv", "w") do io
        @printf(
            io,
            "i, energy, energy variance, inverse participation ratio, overlap\n"
        )
    end
    open("$path/projection.csv", "w") do io
        @printf(io, "")
    end

    # definitions
    t_end = min(t_end, ω*n)
    @show t_end
    t_values = 0:dt:t_end
    Nt = length(t_values)

    # initialize a Gaussian state
    ψ = ψ0 = prepare_gaussian_initial_state(ω, s, n, σ)
    # evolve it in time and make measurements and projections
    for (i, t) in enumerate(t_values)
        ht = h(ω, s, t, n)
        ψ -= (1im * dt) * ht * ψ  # time_propagate(ψ, e, v, dt)
        ψ /= norm(ψ)
        if i % measure_every == 0
            @printf "%d/%d, t=%0.5f\n" i Nt t
            e, v = eigen(ht)
            energy = expectation(e, v, ψ)
            energy_variance = expectation(e.^2, v, ψ) - energy^2
            ipr = inverse_participation_ratio(ψ)
            o = overlap(ψ0, ψ)
            open("$path/measurements.csv", "a") do io
                @printf(io, "%0.10e, %0.10e, %0.10e, %0.10e, %0.10e\n", t,
                        energy, energy_variance, ipr, o)
            end
            projection = adiabatic_project(v, ψ)
            open("$path/projection.csv", "a") do io
                writedlm(io, projection')
            end
        end
    end
end
