# Bloch

include("src/lz.jl")

# run_it(0.0:0.05:100; n=200, ω=0.5, s=1.25664, σ=0.01)

time_evolve(;
    ω=(4 * π / 10),
    s=1.25, # 1.25,
    σ=0.01,
    t_end=20,
    dt=0.00001,
    n=40,
    path="data/bloch2",
    measure_every=1000,
)
