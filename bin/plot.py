import sys
import numpy as np
import matplotlib.pyplot as plt

path = sys.argv[1]
assert(isinstance(path, str))

measurements = np.loadtxt(path + "/measurements.csv", delimiter=",", skiprows=1)
t = measurements[:, 0]
titles = [
    "Energy",
    "Energy variance",
    "Inverse participation ratio",
    r"$|\langle \psi(t_0) | \psi(t) \rangle |^2$"
]
values = [measurements[:, i] for i in range(1, 5)]
for (title, y) in zip(titles, values):
    plt.plot(t, y)
    plt.xlabel(r"$t$")
    plt.title(title)
    plt.savefig(path + "/%s.pdf" % title)
    plt.show()

projection = np.loadtxt(path + "/projection.csv")
nx, ny = projection.shape
# im = np.sqrt(projection[(nx//4):-(nx//4), :(2*ny//4)])
# im = projection[:(2*nx//4), (ny//4):-(ny//4)]
im = np.sqrt(projection)
N = im.shape[1]
plt.imshow(im.T, origin='lower', aspect='auto', extent=(min(t), max(t), -N//2, N//2,))
#, extent=(0, 1500, -100, 100)) for superbloch
# [300:500, :(3*ny//4)]
# extent=(0, 1500, -100, 100)
# (nx//3):-(nx//3), :(3*ny//4)]
plt.xlabel("$t$")
plt.ylabel("n")
plt.colorbar()
plt.title(r"$|\langle \psi(t_) | \psi_n^{(\rm ad)}(t) \rangle |$")
plt.savefig(path + "/projection.pdf")
plt.show()
