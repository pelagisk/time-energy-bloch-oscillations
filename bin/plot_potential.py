import numpy as np
import matplotlib.pyplot as plt


def hamiltonian(omega, J, l, t, n):
    Sz = np.diag(np.arange(-n, n+1))
    N = Sz.shape[0]
    A = np.ones((N, N))
    I = np.diag(np.ones(N))
    i2 = np.array([[1, 0], [0, 1]])
    sz = np.array([[1, 0], [0, -1]])
    sx = np.array([[0, 1], [1, 0]])
    h = omega * np.kron(Sz, i2)
    h += (l*t) * np.kron(I, sz)
    h += J * np.kron(A, sx)
    return h

def hamiltonian2(s, t):
    Sz = np.diag(np.arange(-n, n+1, dtype='float64'))
    N = Sz.shape[0]
    A = np.ones((N, N), dtype='float64')
    I = np.diag(np.ones(N, dtype='float64'))
    i2 = np.array([[1, 0], [0, 1]], dtype='float64')
    sz = np.array([[1, 0], [0, -1]], dtype='float64')
    sx = np.array([[0, 1], [1, 0]], dtype='float64')
    h = np.kron(Sz, i2)
    h += t * np.kron(I, sz)
    h += (s/np.pi)* np.kron(A, sx)
    return h

n = 20
s = 0.1
t_values = np.linspace(0, 4, 1000)
e_values = np.zeros((len(t_values), 2*(2*n + 1)))
for (i, t) in enumerate(t_values):
    e = np.linalg.eigvalsh(hamiltonian2(s, t))
    e_values[i, :] = e

for j in range(2*n + 1):
    plt.plot(t_values, e_values[:, j], 'k-')
plt.xlim(min(t_values), max(t_values))
plt.ylim(-4, 0)
plt.show()
