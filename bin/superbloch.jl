# super-Bloch

# run_it(0.0:0.05:2000; n=200, ω=5.0, s=3.14159, σ=0.01)

include("src/lz.jl")

time_evolve(;
    ω=5.0, #(4 * π / 10),
    s=3.14, # 1.25,
    σ=0.01,
    t_end=2000,
    dt=0.00001,
    n=40,
    path="data/superbloch",
    measure_every=1000,
)
