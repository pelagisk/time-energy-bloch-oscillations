# Time-energy Bloch oscillations

Simulations for the paper "Bloch-like energy oscillations" (Phys. Rev. A 98, 053820) by Axel Gagge and Jonas Larson. Written in Julia, with Python used for plotting.

You can find the paper itself here:

- https://arxiv.org/abs/1808.08061
- https://journals.aps.org/pra/abstract/10.1103/PhysRevA.98.053820

## Requirements and running

Requires the Julia programming language, version 1.0 at least. To generate data files, please run from a terminal with the prompt at the root directory of the project (where this `README.md` file is found)


```bash
> julia bin/bloch.jl
> julia bin/superbloch.jl
```

If you have SSH access to another machine, it is possible to run the computations remotely

```bash
> scp -r $(pwd) <username>@<servername>:~/
> ssh <username>@<servername> "cd ~/${PWD##*/}; nohup julia bin/bloch.jl > /dev/null 2>&1 &"
```

and download the results when they are done

```bash
> scp -r <username>@<servername>:~/${PWD##*/}/data .
```

Finally, figures can be generated using

```bash
> python3 bin/plot.py data/bloch
> python3 bin/plot.py data/superbloch
```

## License

The code is licensed under a [GNU GPL v3 license](https://www.gnu.org/licenses/gpl-3.0.html).
